Postfix
=======

This role install postfix on Trusty as an Internet Site

Requirements
------------

Role Variables
--------------

Dependencies
------------


Example Playbook
----------------


License
-------

ISC

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
