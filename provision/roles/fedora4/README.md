Role Name
=========

This role installs Fedora4

Requirements
------------

Role Variables
--------------


Dependencies
------------

This role expects Java8 and Tomcat in order to run properly

Example Playbook
----------------


License
-------

ISC

Author Information
------------------
