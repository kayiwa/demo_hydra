## Hydra Demo

This repository will set up a hydra head on vagrant with `vagrant up` or will set up an instance hydra with 
`ansible-playbook --limit 1.2.3.4 site.yml -b`

It is important to do the following things to complete those.

### Vagrant steps

make a copy of the `example_site_secrets.yml` with 

```
cp example_site_secrets.yml site_secrets.yml
```

The playbook will be expecting a repository ignored `site_secrets.yml` YAML file. Read the variable contents of the file and adjust accordingly to match your local environment

Run `vagrant up`

Enter the URL of http://192.168.0.14

### Ansible steps

make a copy of the `example_site_secrets.yml` with 

```
cp example_site_secrets.yml site_secrets.yml
```

The playbook will be expecting a repository ignored `site_secrets.yml` YAML file. Read the variable contents of the file and adjust accordingly to match your local environment. You will need the IP address of the server you plan to provision.

Run 

```
ansible-playbook --limit [ip address] -b
```

Enter the URL of http://[ip address ]